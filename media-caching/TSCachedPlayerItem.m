//
//  TSCachedPlayerItem.m
//  media-caching
//
//  Created by Tolik on 10/10/15.
//  Copyright (c) 2015 Tolik Shevchenko. All rights reserved.
//

#import "TSCachedPlayerItem.h"
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>

static NSString *const kTSCustomURLScheme = @"TSCustomURLScheme";
static NSString *const kTSCacherCacheDiskPath = @"TSCacherCacheDiskPath";

#pragma mark - TSCacher implementation (private stuff)

@interface TSCacher : NSObject  <AVAssetResourceLoaderDelegate, NSURLConnectionDataDelegate>
@property (nonatomic, strong) NSString *originalURLScheme;
@property (nonatomic, strong) AVURLAsset *asset;
@property (nonatomic, strong) NSURLConnection *connection;

@property (nonatomic, strong) NSMutableData *mediaData;
@property (nonatomic, strong) NSHTTPURLResponse *response;
@property (nonatomic, strong) NSMutableArray *pendingRequests;
@end

@implementation TSCacher

+ (NSURLCache *)sharedURLCache
{
    static dispatch_once_t predicate = 0;
    static NSURLCache *sharedCache = nil;
    
    dispatch_once(&predicate, ^{
        sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:1024*1024*1024//1GB
                                                    diskCapacity:1024*1024*1024//1GB
                                                        diskPath:kTSCacherCacheDiskPath];
    });
    return sharedCache;
}

- (instancetype)initWithURL:(NSURL *)URL
{
    self = [super init];
    if (nil != self)
    {
        _originalURLScheme = URL.scheme;
        
        NSURLComponents *components = [[NSURLComponents alloc] initWithURL:URL resolvingAgainstBaseURL:NO];
        components.scheme = kTSCustomURLScheme;
        
        _asset = [AVURLAsset URLAssetWithURL:[components URL] options:nil];
        [_asset.resourceLoader setDelegate:self queue:dispatch_get_main_queue()];
        
        _pendingRequests = [[NSMutableArray alloc] init];
    }
    return self;
}

#pragma mark - AVAssetResourceLoaderDelegate implementation

- (BOOL)resourceLoader:(AVAssetResourceLoader *)resourceLoader shouldWaitForLoadingOfRequestedResource:(AVAssetResourceLoadingRequest *)loadingRequest
{
    NSURL *interceptedURL = [loadingRequest.request URL];
    [self.pendingRequests addObject:loadingRequest];
    
    if (self.connection == nil && [interceptedURL.scheme isEqualToString:kTSCustomURLScheme])
    {
        NSURLComponents *actualURLComponents = [[NSURLComponents alloc] initWithURL:interceptedURL resolvingAgainstBaseURL:NO];
        actualURLComponents.scheme = self.originalURLScheme;
        
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[actualURLComponents URL]
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
        
        NSCachedURLResponse *cachedResponse = [[TSCacher sharedURLCache] cachedResponseForRequest:request];
        if (nil == cachedResponse)
        {
            self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
            [self.connection setDelegateQueue:[NSOperationQueue mainQueue]];
            [self.connection start];
        }
        else
        {
            [self processPendingRequestsWithCachedResponse:cachedResponse];
        }
    }
    
    return YES;
}

- (void)resourceLoader:(AVAssetResourceLoader *)resourceLoader didCancelLoadingRequest:(AVAssetResourceLoadingRequest *)loadingRequest
{
    [self.pendingRequests removeObject:loadingRequest];
}

#pragma mark - NSURLConnectionDataDelegate implementation

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.mediaData = [[NSMutableData alloc] init];
    self.response = (NSHTTPURLResponse *)response;
    
    [self processPendingRequests];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.mediaData appendData:data];
    [self processPendingRequests];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSCachedURLResponse *cachedResponse = [[NSCachedURLResponse alloc] initWithResponse:self.response data:self.mediaData];
    [[TSCacher sharedURLCache] storeCachedResponse:cachedResponse forRequest:connection.currentRequest];
    
    [self processPendingRequests];
}

#pragma mark - AVURLAsset resource loading

- (void)processPendingRequestsWithCachedResponse:(NSCachedURLResponse *)cachedResponse
{
    self.mediaData = [cachedResponse.data mutableCopy];
    self.response = (NSHTTPURLResponse *)cachedResponse.response;
    [self processPendingRequests];
}

- (void)processPendingRequests
{
    NSMutableArray *requestsCompleted = [NSMutableArray array];
    for (AVAssetResourceLoadingRequest *loadingRequest in self.pendingRequests)
    {
        [self fillInContentInformation:loadingRequest.contentInformationRequest];
        if ([self respondWithDataForRequest:loadingRequest.dataRequest])
        {
            [requestsCompleted addObject:loadingRequest];
            [loadingRequest finishLoading];
        }
    }
    [self.pendingRequests removeObjectsInArray:requestsCompleted];
}

- (void)fillInContentInformation:(AVAssetResourceLoadingContentInformationRequest *)contentInformationRequest
{
    if (nil == contentInformationRequest || nil == self.response)
    {
        return;
    }
    
    NSString *mimeType = [self.response MIMEType];
    CFStringRef contentType = UTTypeCreatePreferredIdentifierForTag(kUTTagClassMIMEType, (__bridge CFStringRef)(mimeType), NULL);
    
    contentInformationRequest.byteRangeAccessSupported = YES;
    contentInformationRequest.contentType = CFBridgingRelease(contentType);
    contentInformationRequest.contentLength = [self.response expectedContentLength];
}

- (BOOL)respondWithDataForRequest:(AVAssetResourceLoadingDataRequest *)dataRequest
{
    long long startOffset = dataRequest.requestedOffset;
    if (0 != dataRequest.currentOffset)
    {
        startOffset = dataRequest.currentOffset;
    }
    
    // Don't have any data at all for this request
    if (self.mediaData.length < startOffset)
    {
        return NO;
    }
    
    // This is the total data we have from startOffset to whatever has been downloaded so far
    NSUInteger unreadBytes = self.mediaData.length - (NSUInteger)startOffset;
    
    // Respond with whatever is available if we can't satisfy the request fully yet
    NSUInteger numberOfBytesToRespondWith = MIN((NSUInteger)dataRequest.requestedLength, unreadBytes);
    
    [dataRequest respondWithData:[self.mediaData subdataWithRange:NSMakeRange((NSUInteger)startOffset, numberOfBytesToRespondWith)]];
    
    long long endOffset = startOffset + dataRequest.requestedLength;
    BOOL didRespondFully = self.mediaData.length >= endOffset;
    
    return didRespondFully;
}

@end

#pragma mark - TSCachedPlayerItem implementation

@interface TSCachedPlayerItem()
@property (nonatomic, strong) TSCacher *mediaCacher;
@end

@implementation TSCachedPlayerItem

+ (instancetype)cachedPlayerItemWithURL:(NSURL *)URL
{
    return [[self alloc] initWithCachedMediaURL:URL];
}

- (instancetype)initWithCachedMediaURL:(NSURL *)URL
{
    TSCacher *cacher = [[TSCacher alloc] initWithURL:URL];
    
    self = [super initWithAsset:cacher.asset];
    if (nil != self)
    {
        _mediaCacher = cacher;
    }
    return self;
}

@end
