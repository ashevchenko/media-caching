//
//  ViewController.m
//  media-caching
//
//  Created by Tolik on 10/10/15.
//  Copyright (c) 2015 Tolik Shevchenko. All rights reserved.
//

#import "ViewController.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import "TSCachedPlayerItem.h"

static NSString *const kTSMp3SourceURL = @"https://cs7-4v4.vk-cdn.net/p23/a6251ce588aef3.mp3";
static NSString *const kTSSmallMp4SourceURL = @"http://www.ex.ua/load/199349048?fs_id=133";
static NSString *const kTSBigMp4SourceURL = @"http://sample-videos.com/video/mp4/720/big_buck_bunny_720p_50mb.mp4";
static NSString *const kTSM3u8SourceURL = @"http://devimages.apple.com/iphone/samples/bipbop/bipbopall.m3u8";

@interface ViewController ()
@property (nonatomic, strong) AVPlayerViewController *playerViewController;
@end

@implementation ViewController

#pragma mark - IBActions

- (IBAction)onUncachedMp3Tap:(id)sender
{
    NSURL *URL = [NSURL URLWithString:kTSMp3SourceURL];
    [self playUncachedMediaWithURL:URL];
}

- (IBAction)onUncachedSmallMp4Tap:(id)sender
{
    NSURL *URL = [NSURL URLWithString:kTSSmallMp4SourceURL];
    [self playUncachedMediaWithURL:URL];
}

- (IBAction)onUncachedBigMp4Tap:(id)sender
{
    NSURL *URL = [NSURL URLWithString:kTSBigMp4SourceURL];
    [self playUncachedMediaWithURL:URL];
}

- (IBAction)onUncachedM3u8Tap:(id)sender
{
    NSURL *URL = [NSURL URLWithString:kTSM3u8SourceURL];
    [self playUncachedMediaWithURL:URL];
}

- (IBAction)onCachedMp3Tap:(id)sender
{
    NSURL *URL = [NSURL URLWithString:kTSMp3SourceURL];
    [self playCachedMediaWithURL:URL];
}

- (IBAction)onCachedSmallMp4Tap:(id)sender
{
    NSURL *URL = [NSURL URLWithString:kTSSmallMp4SourceURL];
    [self playCachedMediaWithURL:URL];
}

- (IBAction)onCachedBigMp4Tap:(id)sender
{
    NSURL *URL = [NSURL URLWithString:kTSBigMp4SourceURL];
    [self playCachedMediaWithURL:URL];
}

- (IBAction)onCachedM3u8Tap:(id)sender
{
    NSURL *URL = [NSURL URLWithString:kTSM3u8SourceURL];
    [self playCachedMediaWithURL:URL];
}

#pragma mark -

- (AVPlayerViewController *)playerViewController
{
    if (nil == _playerViewController) {
        _playerViewController = [[AVPlayerViewController alloc] init];
    }
    return _playerViewController;
}

- (void)playUncachedMediaWithURL:(NSURL *)URL
{
    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:URL];
    self.playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
    [self presentViewController:self.playerViewController animated:YES completion:nil];
}

- (void)playCachedMediaWithURL:(NSURL *)URL
{
    TSCachedPlayerItem *playerItem = [TSCachedPlayerItem cachedPlayerItemWithURL:URL];
    self.playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
    [self presentViewController:self.playerViewController animated:YES completion:nil];
}

@end
