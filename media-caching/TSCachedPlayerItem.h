//
//  TSCachedPlayerItem.h
//  media-caching
//
//  Created by Tolik on 10/10/15.
//  Copyright (c) 2015 Tolik Shevchenko. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@interface TSCachedPlayerItem : AVPlayerItem
+ (instancetype)cachedPlayerItemWithURL:(NSURL *)URL;
@end
